﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using System.Linq;

namespace StarWarConsole
{
    class Program
    {
        private static string Url = "https://swapi.co/api/starships";  ///?page=4

        static async Task Main(string[] args)
        {
           
            RegisterServices();
            bool isContinue = true;
            while (isContinue)
            {
                Console.WriteLine("<-- Put a number here as passengers number. Put non-numeric data to exit. -->");
                string answer = Console.ReadLine();

               if (float.TryParse(answer, out float number))
                {
                    if (number > 0 )
                    {
                        await RunSampleAsync(number);
                    } else
                    {
                        Console.WriteLine("Please input a number larger than 0.");
                    }
                    
                } else
                {
                    isContinue = false;
                }
                               
            }
            
            Console.WriteLine("Completed");
            Console.ReadLine();
        }

        static async Task RunSampleAsync(float number)
        {
            var controller = AppServices.GetService<StarShipController>();
             var ships = await controller.GetAllShipsAsync(Url);

            var pair = from r in ships
                       from PilotUrl in r.Pilots
                       where Convert.ToInt64(r.Passengers) >= number
                       select new{ r.Name, PilotUrl };


            foreach (var i in pair)
            {
                var pilotName = await controller.GetPilotAsync(i.PilotUrl);
                Console.WriteLine("{0}  -  {1}", i.Name, pilotName);
            }

        }

        static void RegisterServices()
        {
            var services = new ServiceCollection();
            services.AddLogging(builder =>
            {
                builder.AddEventLog();
                builder.AddConsole();
#if DEBUG
                builder.AddDebug();
#endif
                
            });
            services.AddScoped<StarShipController>();
            AppServices = services.BuildServiceProvider();
        }

        public static IServiceProvider AppServices { get; private set; }
    }
}
