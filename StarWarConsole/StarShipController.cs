﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace StarWarConsole
{
    class StarShipController
    {
        private readonly ILogger<StarShipController> _logger;
        public StarShipController(ILogger<StarShipController> logger)
        {
            _logger = logger;
         //   _logger.LogTrace("ILogger injected into {0}", nameof(StarShipController));
        }

        public object LoggingEvents { get; private set; }

        public async Task<List<StarShip>> GetAllShipsAsync(string url)
        {
            try
            {
            //    _logger.LogInformation("GetAllShipsAsync started with url {0}", url);
                var client = new HttpClient();
                List<StarShip> ships = new List<StarShip>();
                string isNext = url ;
                do
                {
                    string result = await client.GetStringAsync(isNext);
                    var item = JsonConvert.DeserializeObject<GetStarShipResponse>(result);
                    isNext = item.Next;
                    ships.AddRange(item.Results);
                } while (isNext != null);                          
               
               foreach (var ship in ships)
                {
                    if (!Int64.TryParse(ship.Passengers, out long number))
                    {
                        ship.Passengers = "-1";
                    }
                }

               // _logger.LogInformation("GetAllShipsAsync completed");

                return ships;

               
               
            }
            catch (Exception ex)
            {
                _logger.LogError( ex, "Error in GetAllShipsAsync, error message: {0}, HResult: {1}", ex.Message, ex.HResult);
                return null;
            }
        }

        public async Task<string> GetPilotAsync(string url)
        {
            try
            {
              //  _logger.LogInformation("GetPilotAsync started with url {0}", url);
                var client = new HttpClient();
                              
                    string result = await client.GetStringAsync(url);
                    var item = JsonConvert.DeserializeObject<People>(result);                                 

            //    _logger.LogInformation("GetPilotAsync completed");

                return item.Name;

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error in GetPilotAsync, error message: {0}, HResult: {1}", ex.Message, ex.HResult);
                return null;
            }
        }


    }
}
